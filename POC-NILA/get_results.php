

  <!DOCTYPE html>
  <html>
  <head>
    <title>Basic Search form using mysqli</title>
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  </head>
  <body>
  <div> 
      <label>Search</label>
      <form action="" method="GET">
          <input type="text" placeholder="Type the name here" name="search">&nbsp;
          <input type="submit" value="Search" name="btn" class="btn btn-sm btn-primary">
      </form>
      
  </div>
   
  </body>
  </html>
  <?php 
  
$localhost  = "localhost"; 
$username   = "root"; 
$password   = ""; 
$dbname     = "new";  
$con = new mysqli($localhost, $username, $password, $dbname); 
if( $con->connect_error){
   die('Error: ' . $con->connect_error);
} 

if( isset($_GET['search']) ){  
  $name = mysqli_real_escape_string($con, htmlspecialchars($_GET['search'])); 
  $res = preg_replace("/[^a-zA-Z]/", "", $name);
  //$sql = "SELECT * FROM searchengine WHERE firstname ='$name' OR lastname ='$name' OR contact ='$name' OR address ='$name'"; 
  $sql = mysqli_query(
    $con,
    sprintf(
        "SELECT * FROM searchengine WHERE keywords LIKE '%s' LIMIT 0,20",
        '%'. $res .'%'
    )
);
if (!$sql) {
    printf("Error: %s\n", mysqli_error($con));
    exit();
}
while($ser = mysqli_fetch_array($sql)) {
  
    echo "<a href='$ser[url]'>$ser[title]</a>";
    
    echo "<br>";
    echo "$ser[description]";
    echo "<br>";
    echo "<br>";
}
} 

?>